This is the HTML Boilerplate for Starting the new scratch project.

This project contains the bootstrap, sass & Gulp.

Using GULP we are optimizing the project at Frontend Level.

Clone this project and rename it.
After rename the project first install the gulp in your machine globally. ignore this if already install.

Run the below command from the root directory of the project.
"npm install"
it will download the all required dependency.

After "npm install" run "gulp" command at the root directory of the project. if you have install gulp properly the this command should work. if not the please check gulp installation and also check that you have installed the required gulp plugins.

Check the gulpfile.js for gulp plugins. gulp plugins names are mentioned at the top. Just to verify that plugins are installed or not, check the "node_modules" folder. you will find the folders with the same plugins name.

if you don't find the plugin folder then install all plugin manually by running this command.
"npm install gulp-watch"
gulp-watch is plugin name. Install all plugin same way, then run the gulp command.

For more info please visit the Gulp site: https://gulpjs.com/

This project contains SASS as well. So we have to write the all code in to scss file rest of the work is handled by the gulp.
Gulp will convert the scss file in css file and will merger the all other css libraries and will provide the minified version of the css as well js.

We will use minified versions while developing and deploying time.

For more info kindly contact to mehul.golania@acceleratebs.com